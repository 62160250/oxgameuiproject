/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bug.oxgameui;

import java.io.Serializable;

/**
 *
 * @author OS
 */
public class Table implements Serializable{

    private char[][] table
            = {{'-', '-', '-'},
            {'-', '-', '-'},
            {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean Finish = false;
    private int count = 0;
    private char winner2 = '-';
    private int LastCol;
    private int lastRow;

    public Table(Player X, Player O) {
        playerX = X;
        playerO = O;
        currentPlayer = X;

    }

    public void showTable() {
        System.out.println("  1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1 + " ");
            for (int l = 0; l < table[i].length; l++) {
                System.out.print(table[i][l] + " ");
            }
            System.out.println(" ");
        }
    }

    public char getRowCol(int row, int col) {
        return table[row][col];
    }

    public boolean setRowCol(int row, int col) {
        if(isFinish()) return false;
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.LastCol = col;
            count++;
            checkWin();
            return true;
        }
        return false;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlyer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][LastCol] != currentPlayer.getName()) {
                return;
            }
        }
        Finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastRow][col] != currentPlayer.getName()) {
                return;
            }
        }
        Finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    void checkX() {
        if (table[0][0] == table[1][1]
                && table[1][1] == table[2][2]
                && table[0][0] != '-') {
            Finish = true;
            winner = currentPlayer;
            setStatWinLose();
        }
        if (table[0][2] == table[1][1]
                && table[1][1] == table[2][0]
                && table[0][2] != '-') {
            Finish = true;
            winner = currentPlayer;
            setStatWinLose();
        }
        return;
    }

    void checkDraw() {
        if (count == 8) {
            this.showTable();
            Finish = true;
            playerO.draw();
            playerX.draw();
        }
    }

    public void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkDraw();
    }

    public boolean isFinish() {
        return Finish;
    }

    public Player getWinner() {
        return winner;
    }

}
